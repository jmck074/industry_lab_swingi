package ictgradschool.industry.lab_swingi.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener{
    private JTextField firstTextField;
    private JTextField secondTextField;
    private JButton addButton;
    private JButton subtractButton;
    private JTextField resultTextField;
    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);
        firstTextField = new JTextField(10);
        secondTextField = new JTextField(10);
        addButton = new JButton("Add");
        subtractButton = new JButton("Subtract");
        resultTextField = new JTextField(20);

        this.add(firstTextField);
        this.add(secondTextField);
        this.add(addButton);
        this.add(subtractButton);
        this.add(resultTextField);

        addButton.addActionListener(this);
        subtractButton.addActionListener(this);

    }

    public void actionPerformed(ActionEvent event){
        double firstNumber = Double.parseDouble(firstTextField.getText());
        double secondNumber = Double.parseDouble(secondTextField.getText());
        if(event.getSource()==addButton){

            resultTextField.setText(Double.toString(roundTo2DecimalPlaces(firstNumber+secondNumber)));
        }
        else if(event.getSource()==subtractButton){
            resultTextField.setText(Double.toString(roundTo2DecimalPlaces(firstNumber-secondNumber)));
        }

    }
    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}