package ictgradschool.industry.lab_swingi.ex03;

import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // TODO Draw a house, as shown in the lab handout.
        g.setColor(MAIN_COLOR);
        g.fillRect(left, top+(size*5), size*10, size*7);
        g.setColor(DOOR_COLOR);
        g.fillRect(left+(size*4), top+(size*8), size*2, size*4);
        g.setColor(WINDOW_COLOR);
        g.fillRect(left+size, top+size*7, size*2, size*2);

        g.fillRect(left+7*size, top+size*7, size*2, size*2);

        int[]triangle_x= {left, left+size*5, left+size*10};
        int[]triangle_y= {top+5*size, top, top+5*size};
        g.setColor(ROOF_COLOR);
        g.fillPolygon(triangle_x,triangle_y, 3 );
        int[] chimney_x={left+7*size,left+7*size, left+8*size, left+8*size};
        int[] chimney_y={top+2*size, top+size, top+size, top+3*size};

        g.setColor(CHIMNEY_COLOR);
        g.fillPolygon(chimney_x,chimney_y, 4);


        g.setColor(OUTLINE_COLOR);
        g.drawRect(left, top+(size*5), size*10, size*7);
        g.drawRect(left+(size*4), top+(size*8), size*2, size*4);
        g.drawRect(left+size, top+size*7, size*2, size*2);
        g.drawRect(left+7*size, top+size*7, size*2, size*2);
        g.drawLine(left+size, top+size*8, left+size*3,top+size*8);
        g.drawLine(left+size*2, top+size*7, left+size*2,top+size*9);
        g.drawLine(left+size*7, top+size*8, left+size*9,top+size*8);
        g.drawLine(left+8*size, top+7*size, left+8*size, top+9*size);
        g.drawPolygon(triangle_x,triangle_y, 3 );
        g.drawPolygon(chimney_x,chimney_y, 4);
    }

}