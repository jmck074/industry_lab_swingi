package ictgradschool.industry.lab_swingi.ex04;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import static java.awt.event.KeyEvent.*;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener{

    private  Balloon balloon;
    //private  JButton moveButton;
    private Timer timer;
    private Balloon[] ninetyEightRed;
    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        this.balloon = new Balloon(30, 60);
        ninetyEightRed=new Balloon [98];
        for(int i=0; i<ninetyEightRed.length;i++){
            int leftRandom=(int)(Math.random()*800);
            int topRandom=(int)(Math.random()*800);
            Balloon b=new Balloon(leftRandom,topRandom);
            ninetyEightRed[i]=b;
        }
        //this.moveButton = new JButton("Move balloon");
        //this.moveButton.addActionListener(this);
        //this.add(moveButton);
        this.addKeyListener(this);
        this.timer = new Timer(200, this);
    }

    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        balloon.move();
        for(Balloon b: ninetyEightRed){
            b.move();
        }
        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        balloon.draw(g);
        for(Balloon b: ninetyEightRed){
            b.draw(g);
        }
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
    if(timer.isRunning()==false){timer.start();}

        if(e.getKeyCode() == VK_UP){
        balloon.setDirection(Direction.Up);
        for(Balloon b: ninetyEightRed){
            b.setDirection(Direction.Up);
        }

    }
    else if(e.getKeyCode()==VK_DOWN){
        balloon.setDirection(Direction.Down);
    }
    else if(e.getKeyCode()==VK_LEFT){
        balloon.setDirection(Direction.Left);
    }
    else if(e.getKeyCode()==VK_RIGHT){
        balloon.setDirection(Direction.Right);
    }
    else if(e.getKeyCode()==VK_S){
        timer.stop();
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {

    }
}